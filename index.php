<?php declare(strict_types = 1);
require_once 'functions.php';
require_once 'FileException.php';
include 'parts/header.php';

const FILE_FOLDER = 'files'; //pradinis failu folderis
const IMAGE_FILE_EXTENSIONS = ['png', 'jpeg', 'jpg'];
?>

<?php   
    if (isset($_FILES['uploaded_file'])) {
        if ($_FILES['uploaded_file']['tmp_name']) {
            try {
                $message = upload($_FILES['uploaded_file']);
            } catch (FileException $e) {
                $message = $e->getMessage();
                              
            } catch (\Exception $e) {
                $message = "Įvyko klaida.";
            }
        } else {
            $message = "Unable to upload file";
        }?>
        <div class="container">
       <?php include 'parts/message.php'; ?>
        </div> <?php
    }?>

<div class="container">

    <h1 class="navbar-brand mb-0 h1">Failų įkėlimas</h1>
    <form method="post" enctype="multipart/form-data">
        <input type="file" name="uploaded_file">
        <input type="submit" value="Upload">        
    </form>

    <h1 class="navbar-brand mb-0 h1">Failai</h1>
    <form>
        <div class="form-group">
            <ul class="list-group">
               <?php listFiles(FILE_FOLDER); ?>               
            </ul>
        </div>
    </form> 
      
</div>
<br>

<?php

include 'parts/footer.php';?>
 