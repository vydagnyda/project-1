<?php declare(strict_types = 1);
require_once 'config.php';

function listFiles(string $directory, int $level = 0): void 
{
$dir = opendir($directory);
while ( $filename = readdir($dir) ) {   
    if (is_dir($directory . '/' . $filename)) {        
        if ($filename != '..' && $filename != '.') {
            echo '<li class="list-group-item">'; 
            for ($i = 0; $i <= $level; $i++) {
                if ($i > 0){
                    echo '<i class="fas fa-angle-right"></i> &nbsp;&nbsp;';
                }
            }
            echo '<i class="far fa-folder-open"></i> ' . $filename;  
            echo  '</li> ';          
            listFiles($directory. '/' . $filename, $level + 1);
        }             
    }   

    if (is_file($directory . '/' . $filename)) {
        $pathInfo = pathinfo($filename);
        $extension = $pathInfo['extension'];
        echo '<li class="list-group-item">'; 
        for ($i = 0; $i <= $level; $i++) {
            if ($i > 0){
                echo '<i class="fas fa-angle-right"></i> &nbsp;&nbsp;';
            }
        }
        if ($extension == 'php') {
            echo '<a href="readfile.php?file=' . $directory . '/' . $filename . '" />'
                .'<i class="far fa-file-powerpoint"></i>'
                . $filename
                . '</a>'
            ;
        }

        if ($extension == 'txt') {
            echo '<a href="readfile.php?file=' . $directory . '/' . $filename . '" />'
                . '<i class="far fa-file-alt"></i>'
                . $filename               
                . '</a>'
            ;
        }       

        if (in_array($extension, IMAGE_FILE_EXTENSIONS)) {
            echo '<a href="viewImage.php?file=' . $directory . '/' . $filename . '" />'
                . '<i class="far fa-file-image"></i>'
                . $filename        
                . '</a>'
            ;
        }
        echo  '</li> ';   
    }   
}
closedir ( $dir );
}


function isFileSizeAllowed(array $file): bool
{
    return filesize($file['tmp_name']) < MAX_FILE_SIZE;
}

function isExtentionAllowed(array $file): bool
{
    $extention = pathinfo($file['name'], PATHINFO_EXTENSION);

    return in_array($extention, ALLOWED_EXTENSIONS);
}

function filterFileName(string $oldFileName): string
{
    $result = filter_var($oldFileName, FILTER_SANITIZE_STRING);
    $result = str_replace(' ', '_', $result); //tarpus pakeicia i underscore'us
    $result = strtolower($result);
    $result = preg_replace('/[^a-z0-9_\.]/i', '', $result);

    return $result;
}

function upload(array $file): string
{
    if (!isFileSizeAllowed($file)) {
        throw new FileException('File size is too large');
        $message = 'File size is too large';
        return $message;
    }

    if (!isExtentionAllowed($file)) {
        throw new FileException('File extention is not allowed');
        $message = 'File extention is not allowed';
        return $message;
    }

    move_uploaded_file(
        $file['tmp_name'],
        FILE_FOLDER . '/' . filterFileName($file['name'])
    );

    $message = "File successfully uploaded";
    return $message;
}

function isLoggedIn(): bool
{
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }

    return isset($_SESSION['admin_logged_in']) && $_SESSION['admin_logged_in'] == true;
}

function checkLoginAndRedirect(): void
{
    if (!isLoggedIn()) {
        header('Location:' . BASE_URL . '/login.php');
        addFlashMessage('danger', 'You must be logged in!');
        exit();
    }
}

function checkPassword(string $password): bool
{
    return password_verify($password, ADMIN_PASSWORD_HASH);
}

function login(): void
{
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }

    $_SESSION['admin_logged_in'] = true;
}

function logout(): void
{
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }

    session_destroy();
}

function addFlashMessage(string $messageType, string $text)
{
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }

    if (!isset($_SESSION['flash_messages'])) {
        $_SESSION['flash_messages'] = [];
    }

    $_SESSION['flash_messages'][] = [
        'type' => $messageType,
        'text' => $text,
    ];
}

