<?php require_once 'functions.php';?>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css" />
</head>

<body>     

    <nav class="navbar navbar-light bg-light">        
        <span class="navbar-brand mb-0 h1 ">Failų naršymo sistema</span>            
    
      <?php if (isLoggedIn()) {?> 
            <span><a href="<?php echo BASE_URL; ?>/actions/logout.php">Logout</a></span>
            <?php }?>
    </nav>
    <br> 
    <div class="container"> 
    <?php
    if (isset($_SESSION['flash_messages'])) {
        foreach ($_SESSION['flash_messages'] as $message) {
            $messageType = $message['type'];
            $message = $message['text'];
            include 'message.php';
        }
        unset($_SESSION['flash_messages']);
    }
    ?>
    </div>

</body>
</html>
