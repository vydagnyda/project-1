<?php
const ADMIN_PASSWORD_HASH = '$2y$10$GWO0nCQygkTeXFA8HioCS.zPn/x9Lw6kwPTDDduKN8cLzLdO8heVe';
const BASE_URL = 'http://localhost:90/project-1';
const MAX_FILE_SIZE = 525288; //0.5mb
const ALLOWED_EXTENSIONS = ['jpg', 'jpeg', 'png', 'php', 'txt'];
